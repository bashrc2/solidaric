# Solidaric Font

By Bob Mottram <bob@freedombone.net>

This font is based upon the typeface used in the Mother Earth magazine from March 1909, edited by Emma Goldman. It is licensed under the GNU Affero General Public License version 3.

![screenshot](screenshot.png)
